console.log('correcto');
	document.querySelector('#boton').addEventListener('click', traerDatos);
	var ide = document.getElementById("ident");

	function traerDatos(){
	    const xhttp = new XMLHttpRequest();
	    console.log(ide.value);
	    xhttp.open('GET', 'http://localhost:8080/'+ ide.value, true);

	    xhttp.send();

	    xhttp.onreadystatechange = function(){
	        if(this.readyState == 4 && this.status == 200){	            
	        	try{
	        		console.log("asignar");
	        		var datos = JSON.parse(this.responseText);
	        	}catch(err){
	        		alert("Error en la consulta, por favor verifica el número ingresado");
	        	}
	        	
	            let res = document.querySelector('#res');
	            res.innerHTML = '';            
	                res.innerHTML += `                
	                <thead>
					    <tr>
						    <th>Dato</th>
	                        <th>Valor</th>
					    </tr>
				    </thead>
				    <tbody >
	                    <tr>
	                        <td>
	                        <b>Número de identificación:</b>                          
	                        </td>
	                        <td align="right">
	                            ${datos.id} 
	                        </td>
	                    </tr>
	                    <tr>
	                        <td>
	                            <b>Nombre:</b>
	                        </td>
	                        <td align="right">
	                            ${datos.nombre} 
	                        </td>
	                    </tr>
	                    <tr>
	                        <td>
	                            <b>Dirección:</b>
	                        </td>
	                        <td align="right">
	                            ${datos.direccion} 
	                        </td>
	                    </tr>
	                    <tr>
	                        <td>
	                            <b>Ciudad:</b>
	                        </td>
	                        <td align="right">
	                            ${datos.ciudad} 
	                        </td>
	                    </tr>
	                    <tr>
	                        <td>
	                            <b>Teléfono:</b>
	                        </td>
	                        <td align="right">
	                            ${datos.telefono} 
	                        </td>
	                    </tr>
	                    <tr>
	                        <td>
	                            <b>Género:</b>
	                        </td>
	                        <td align="right">
	                            ${datos.genero} 
	                        </td>
	                    </tr>
	                    <tr>
	                        <td>
	                            <b>Profesión:</b>
	                        </td>
	                        <td align="right">
	                            ${datos.profesion} 
	                        </td>
	                    </tr>
				    </tbody>
	                    
	                `
	            }else if (this.status == 400 || this.status == 404){
	            	alert("Error en la consulta, por favor verifica el número ingresado");
	            }
	        	        
	    }

	}