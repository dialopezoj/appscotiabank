package com.example.scotiabank;

import java.util.ArrayList;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class DatosController {
	private ArrayList<Cliente> listaClientes = null;
	public DatosController() {
		listaClientes = new ArrayList<Cliente>();
		listaClientes.add(new Cliente(1025365224, "Camilo Ramirez", "Calle 4 No. 5 -60", "Medellín", "3152635214", "Masculino", "Médico"));
		listaClientes.add(new Cliente(42563741, "Esperanza Morales", "Carerra 15 No. 22 -45", "Bogotá", "3108547854", "Femenino", "Psicóloga"));
		listaClientes.add(new Cliente(19354852, "Mauricio Anaya", "Calle 85 No. 6 -70", "Cali", "318999254", "Masculino", "Ingeniero"));
		listaClientes.add(new Cliente(45213523, "Ana María Cortes", "Av 72 No. 102 - 60", "Bucaramanga", "3205521214", "Femenino", "Arquitecta"));		
	}
	
		
	@GetMapping(value="/{id}", produces = "application/json")
    public Cliente consultarDatos(@PathVariable("id") int id) {
        Cliente resultado = null;
        for(Cliente c: listaClientes) {
        	if(c.getId()==id) {
        		resultado = c;
        	}
        }
        return resultado;
    }	

}
